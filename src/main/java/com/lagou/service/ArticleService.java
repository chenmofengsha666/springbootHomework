package com.lagou.service;

import com.lagou.bean.Article;
import com.lagou.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ArticleService {

    @Autowired
    private ArticleRepository articleRepository;
    public Page<Article> list(Pageable pageable){

        Page<Article> result = articleRepository.findAll(pageable);
        return result;
    }
}
