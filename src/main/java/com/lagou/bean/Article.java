package com.lagou.bean;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity(name = "t_article")
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String title;
    private String content;
    private Date created;
    private Date modified;
    private String categories;
    private String tags;
    @Column(name="allow_comment")
    private int allowComment;
    private String thumbnail;


}
